<?php
/**
 * khpt functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage khpt
 * @since 1.0.0
 */
?>
<?php


if (!function_exists('khpt_setup')) :

    function khpt_setup()
    {

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1568, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary-menu' => __('Primary', 'khpt')
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'height' => 100,
                'width' => 230,
                'flex-width' => false,
                'flex-height' => false,
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Add support for full and wide align images.
        add_theme_support('align-wide');

        // Add support for editor styles.
        add_theme_support('editor-styles');

        // Enqueue editor styles.
        add_editor_style('style-editor.css');

        // Add custom editor font sizes.
        add_theme_support(
            'editor-font-sizes',
            array(
                array(
                    'name' => __('Small', 'khpt'),
                    'shortName' => __('S', 'khpt'),
                    'size' => 14,
                    'slug' => 'small',
                ),
                array(
                    'name' => __('Normal', 'khpt'),
                    'shortName' => __('M', 'khpt'),
                    'size' => 20,
                    'slug' => 'normal',
                ),
                array(
                    'name' => __('Large', 'khpt'),
                    'shortName' => __('L', 'khpt'),
                    'size' => 22,
                    'slug' => 'large',
                ),
                array(
                    'name' => __('Huge', 'khpt'),
                    'shortName' => __('XL', 'khpt'),
                    'size' => 25,
                    'slug' => 'huge',
                ),
            )
        );

        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');
    }
endif;
add_action('after_setup_theme', 'khpt_setup');
function arphabet_widgets_init()
{

    register_sidebar(array(
        'name' => 'Home right sidebar',
        'id' => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'arphabet_widgets_init');

//Footer widget area
register_sidebar(array(
    'name' => 'Footer Area 1',
    'id' => 'footer-1',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h5 class="widget-title">',
    'after_title' => '</h5>',
));

register_sidebar(array(
    'name' => 'Footer Area 2',
    'id' => 'footer-2',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h5 class="widget-title">',
    'after_title' => '</h5>',
));

register_sidebar(array(
    'name' => 'Footer Area 3',
    'id' => 'footer-3',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h5 class="widget-title">',
    'after_title' => '</h5>',
));

register_sidebar(array(
    'name' => 'Footer Area 4',
    'id' => 'footer-4',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h5 class="widget-title">',
    'after_title' => '</h5>',
));
register_sidebar(array(
    'name' => 'Footer Area 5',
    'id' => 'footer-5',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h5 class="widget-title">',
    'after_title' => '</h5>',
));
// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.

/*---------enqueue custom style sheet----------*/
function load_stylesheets()
{

    //Bootstrap.min.css
    wp_register_style('bootstrap.min.css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.5.0', 'all');
    //All.min.css
    wp_register_style('font-awesome.min.css', get_template_directory_uri() . '/assets/fonts/font-awesome.min.css', array(), '5.12.0', 'all');
     //All.min.css
     wp_register_style('fontawesome5-overrides.min.css', get_template_directory_uri() . '/assets/fonts/fontawesome5-overrides.min.css', array(), '5.12.0', 'all');
          //All.min.css
  wp_register_style('fontawesome-all.min.css', get_template_directory_uri() . '/assets/fonts/fontawesome-all.min.css', array(), '5.12.0', 'all');
    // owl.carousel.min.css
    wp_register_style('cards.css', get_template_directory_uri() . '/assets/css/cards.css', array(), '4.5.0', 'all');
    //owl.theme.default.css
    wp_register_style('slick.css', get_template_directory_uri() . '/assets/css/slick.css', array(), '2.3.4', 'all');
    //jquery.fancybox.min.css
    wp_register_style('slick-theme.css', get_template_directory_uri() . '/assets/css/slick-theme.css', array(), '1.0.2', 'all');
    //animate.css
    wp_register_style('swiper.min.css', get_template_directory_uri() . '/assets/css/swiper.min.css', array(), '5.2.1', 'all');
    wp_register_style('aos.css', get_template_directory_uri() . '/assets/css/aos.css', array(), '2.1.1', 'all');
    //animate.css
    wp_register_style('animate.min.css', get_template_directory_uri() . '/assets/css/animate.min.css', array(), '3.5.2', 'all');
    //Stylesheet
    wp_register_style('style.css', get_template_directory_uri() . '/style.css', array(
        'bootstrap.min.css',
        'cards.css',
        'slick.css',
        'slick-theme.css',
        'swiper.min.css',
        'fontawesome-all.min.css',
        'fontawesome5-overrides.min.css',
        'font-awesome.min.css',
        'aos.css',
        'animate.min.css',
    ), '1.25.1', 'all');
    wp_enqueue_style('style.css');


}

add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_scripts()
{
    wp_register_script('bootstrap.min.js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '4.5.0', 1);
    //bootstrap.min.js
    wp_register_script('jquery.min.js', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '3.5.1', 1);
    //owl.carousel.min.js
    wp_register_script('slick.js', get_template_directory_uri() . '/assets/js/slick.js', array(), '2.3.4', 1);
    //slick.js
    wp_register_script('slick.min.js', get_template_directory_uri() . '/assets/js/slick.min.js', array(), '1.8.1', 1);
      //slick.js
      wp_register_script('swiper.min.js', get_template_directory_uri() . '/assets/js/swiper.min.js', array(), '1.8.1', 1);
       //slick.js
       wp_register_script('jquery.lettering.js', get_template_directory_uri() . '/assets/js/jquery.lettering.js', array(), ' 0.7.0', 1);
    //init.js
    wp_register_script('aos.js', get_template_directory_uri() . '/assets/js/aos.js', array(), ' 2.1.1', 1);
    //init.js
    wp_register_script('init.js', get_template_directory_uri() . '/assets/js/init.js', array(
        'slick.js',
        'jquery.min.js',
        'bootstrap.min.js',
        'slick.min.js',
        'swiper.min.js',
        'jquery.lettering.js',
        'aos.js',
    ), 1.66, 0);
    wp_enqueue_script('init.js');

}

add_action('wp_enqueue_scripts', 'load_scripts');

function kh_scripts() {
    wp_localize_script('init.js', 'my_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_enqueue_scripts', 'kh_scripts');


/**
 * Our Team
 */
function get_dealer() {
    $channel = sanitize_text_field($_POST['data']['city']);
    $posts = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'dealer-locator',
            'meta_key'   => 'city',
            'meta_value' => $channel,
            'fields' => 'post_title'
        )
    );

    $dealer = [];
    foreach ($posts  as $cat) {
        $city = get_field("city", $cat);
        $state =  get_field("state", $cat);
        $contact =  get_field("contact_no", $cat);
        $dealer_name = $cat->post_title;
        $address =  get_field("address", $cat);
        $isIndustrial = get_field("is-industrial", $cat);

        array_push($dealer,array(
            'city' => $city,
            'State' => $state,
            'Contact' => $contact,
            'Dealer_name' => $dealer_name,
            'Address' => $address,
            'is_industrial' => $isIndustrial
        ));
    }
    echo json_encode($dealer);
    wp_die();
}

add_action('wp_ajax_nopriv_get_dealer', 'get_dealer');
add_action('wp_ajax_get_dealer', 'get_dealer');

/**
 *
 * Add acf options page for Resources
 */
if (function_exists('acf_add_options_page')) {

    acf_add_options_sub_page(array(
        'page_title' => 'Research Banner',
        'menu_title' => 'Research Banner',
        'parent_slug' => 'edit.php?post_type=research',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Our Team Banner',
        'menu_title' => 'Our Team Banner',
        'parent_slug' => 'edit.php?post_type=our-teams',
    ));

}

function load_script() {
    wp_localize_script('init.js', 'my_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'load_script');

function get_city() {
    $channel = sanitize_text_field($_POST['data']['channel']);
    $posts = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'dealer-locator',
            'meta_key'   => 'state',
            'meta_value' => $channel,
            'fields' => 'ids'
        )
    );

    $city = [];
    $category = [];
    foreach ($posts  as $cat) {
        $category[] = get_field("city", $cat);
    }

    sort($category);
    $clength = count($category);
    for($x = 0; $x < $clength; $x++) {
        if (!in_array($category[$x], $city)) {
            array_push($city, $category[$x]);
        }
    }
    echo json_encode(array_unique($city));
    wp_die();
}

add_action('wp_ajax_nopriv_get_city', 'get_city');
add_action('wp_ajax_get_city', 'get_city');





