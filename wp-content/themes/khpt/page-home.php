<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
if (have_posts()) :
   while (have_posts()) : the_post();
?>
      <section data-aos="fade" id="banner">
         <div class="position-relative">
            <div class="banner-slider">
               <?php
               // check if the repeater field has rows of data
               if (have_rows('slider')) :

                  // loop through the rows of data
                  while (have_rows('slider')) : the_row();
               ?>
                     <img class="img-fluid d-none d-md-block" src="<?php the_sub_field('desktop_image'); ?>">
               <?php
                  endwhile;
               endif;
               ?>
            </div>
            <div class="banner-slider">
               <?php
               // check if the repeater field has rows of data
               if (have_rows('slider')) :

                  // loop through the rows of data
                  while (have_rows('slider')) : the_row();
               ?>
                     <img class="img-fluid d-block d-md-none" src="<?php the_sub_field('mobile_image'); ?>">
               <?php
                  endwhile;
               endif;
               ?>
            </div>
            <div class="col-12 col-md-5 banner-bg">
               <div class="banner-text-box">
                  <div class="banner-heading-text">
                     <?php
                     // check if the repeater field has rows of data
                     if (have_rows('slider')) :

                        // loop through the rows of data
                        while (have_rows('slider')) : the_row();
                     ?>
                           <h1 class="banner_heading"><?php the_sub_field('heading'); ?></h1>
                           <?php
                           $target = "_self";
                           if (get_sub_field('open_new_tab')) :
                              $target = "_blank";
                           endif;
                           if (get_sub_field('link_label')) :
                           ?>
                              <a class="btn hvr-float-shadow" role="button" href="<?php the_sub_field('link_url'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('link_label'); ?></a>
                     <?php
                           endif;
                        endwhile;
                     endif;
                     ?>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <style>
         #dna-wrapper {
            background-image: url(<?php the_field('our_unique_dna_background_image'); ?>);
         }
      </style>
      <!-- Unique DNA Start-->
      <section id="unique-dna" class="unique-dna">
         <h1 data-aos="fade-up"><?php the_field('our_unique_dna_heading', false, false); ?></h1>
         <div class="container mt-3 desktop-hide" data-aos="fade-up">
            <div id="dna-wrapper" class="row history-content no-gutters">
               <?php
               // check if the repeater field has rows of data
               if (have_rows('our_unique_dna')) :
                  $i = 0;
                  // loop through the rows of data
                  while (have_rows('our_unique_dna')) : the_row();
               ?>
                     <div id="<?php echo $i; ?>" class="col-sm-4 hover-state click-dna">
                        <img class="img-fluid" src="<?php the_sub_field('image'); ?>">
                        <div class="section-wrap heading-wrap px-4">
                           <?php the_sub_field('content'); ?>
                        </div>
                        <div class="show-dna px-5 py-4">
                           <?php the_sub_field('our_unique_dna_inner_content'); ?>
                        </div>
                     </div>
                     <a class="closeFeature" href="javascript:;" style="display:none;"><span>Text</span></a>
               <?php
                     $i++;
                  endwhile;
               endif;
               ?>
            </div>
         </div>
         <div class="container" data-aos="fade-up" id="mobile-view-dna">
            <div class="row">
               <div class="col-12 mt-3 accordion-wrapper">
                  <div id="accordion" class="accordion-bg" style="background-image: url('<?php the_field('our_unique_dna_mobile_bg'); ?>');">
                     <?php
                     $i = 1;
                     // check if the repeater field has rows of data
                     if (have_rows('our_unique_dna_mobile')) :

                        // loop through the rows of data
                        while (have_rows('our_unique_dna_mobile')) : the_row();
                     ?>
                           <div class="card accordion-content">
                              <div id="heading-<?php echo $i; ?>" class="card-header card-header-wrapper p-0">
                                 <h5 class="mb-0"><button class="btn btn-link collapsed mobile-card-header" type="button" data-toggle="collapse" data-target="#collapse-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $i; ?>">
                                       <i class="fa fa-plus-circle pr-2"></i> <a class="" href="javascript:void(0)"><?php the_sub_field('heading'); ?></a></button>
                                 </h5>
                              </div>
                              <div id="collapse-<?php echo $i; ?>" class="collapse collapse-content" aria-labelledby="headingone" data-parent="#accordion">
                                 <div class="card-body accordion-one" style="background-image: url(<?php the_sub_field('image'); ?>);">
                                    <a class="close-icon collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#collapse-<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $i; ?>"><i class="fa fa-times-circle cancel-icon pr-2"></i></a>
                                    <div class="card-text">
                                       <?php the_sub_field('content'); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                     <?php
                           $i++;
                        endwhile;
                     endif;
                     ?>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Unique DNA End -->
      <!-- Our Focus Start -->
      <?php
      // check if the repeater field has rows of data
      if (have_rows('our_focus')) :
      ?>
         <section id="Our-Focus" class="unique-dna">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <h1 data-aos="fade-up"><?php the_field('our_focus_heading', false, false); ?></h1>
                  </div>
               </div>
               <div class="row mt-3">
                  <div class="col-md-10 offset-md-1 para-focus">
                     <p data-aos="fade-up" class="text-center"><?php the_field('our_focus_content', false, false); ?></p>
                  </div>
               </div>
            </div>
            <div class="container mt-3">
               <div class="row justify-content-center">
                  <?php
                  // check if the repeater field has rows of data

                  // loop through the rows of data
                  while (have_rows('our_focus')) : the_row();
                  ?>
                     <div class="col-12 col-sm-4 col-md-4 focus-content" data-aos="fade-up" data-aos-delay="50">
                        <?php
                        $target = "_self";
                        if (get_sub_field('open_new_tab')) :
                           $target = "_blank";
                        endif;
                        ?>
                        <a href="<?php the_sub_field('link_url'); ?>" target="<?php echo $target; ?>">
                           <div class="card">
                              <div class="card-body p-0"><img class="img-fluid" src="<?php the_sub_field('image'); ?>"></div>
                              <p class="card-text"><?php the_sub_field('heading'); ?></p>
                           </div>
                     </div>
                     </a>
                  <?php
                  endwhile;
                  ?>
               </div>
            </div>
         </section>
      <?php
      endif;
      ?>
      <!-- Our Focus End -->
      <!-- Our Role Start -->
      <section id="our-role" class="d-none d-xl-block">
         <h1 data-aos="fade-up"><?php the_field('our_roles_heading', false, false); ?></h1>
         <div class="container">
            <div data-aos="zoom-in" class="row">
               <div class="col-12 spinner">
                  <svg id="chain-desk-1" class="chain_desk-1" height="500" width="700">
                     <line x1="35000" y1="0" x2="0" y2="400" style="stroke:rgb(51,51,51);stroke-width:2"></line>
                  </svg>
                  <svg id="chain-desk-1" class="chain_desk-2" height="156" width="700">
                     <line x1="0" y1="350" x2="650" y2="0" style="stroke:rgb(51,51,51);stroke-width:2"></line>
                  </svg>
                  <svg id="chain-desk-2" class="chain_desk-3" height="160" width="500">
                     <line x1="900" y1="350" x2="0" y2="70" style="stroke:rgb(51,51,51);stroke-width:2"></line>
                  </svg>
                  <div class="spin-wrap">
                     <div>
                        <img class="img-fluid  our-role-header-img" src="<?php the_field('innovation_inner_image'); ?>">
                        <h6 class="our-role-header-1" width="10ex"><?php the_field('innovation_heading'); ?></h6>
                        <img id="gear1" class="img-fluid spin" src="<?php the_field('innovation_image'); ?>">
                        <i id="gear4" class="fa fa-5x fa-gear spin-left-side"></i>
                        <i id="gear5" class="fa fa-5x fa-gear spin-left-down"></i>
                     </div>
                     <div>
                        <img id="gear2" class="img-fluid spin-back" src="<?php the_field('technical_image'); ?>">
                        <img class="img-fluid our-role-header" src="<?php the_field('technical_inner_image'); ?>">
                        <h6 class="our-role-header-5"><?php the_field('technical_heading'); ?></h6>
                     </div>
                     <div>
                        <img id="gear3" class="img-fluid spin" src="<?php the_field('knowledge_image'); ?>">
                        <img class="img-fluid our-role-header-img-3" src="<?php the_field('knowledge_inner_image'); ?>">
                        <h6 class="our-role-header-3"><?php the_field('knowledge_heading'); ?></h6>
                        <i id="gear6" class="fa fa-5x fa-gear spin-left-side"></i>
                        <i id="gear7" class="fa fa-5x fa-gear spin-left-down"></i>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="our-role-mobile" class="mb-5 unique-dna d-block d-xl-none">
         <h1 data-aos="fade-up">Our<span class="text-orange">&nbsp;Role</span></h1>
         <div class="container">
            <div data-aos="zoom-in" class="row">
               <div class="col-12">
                  <div class="row">
                     <div id="gear-1-mobile" class="col-12 spinner gear-1-spinner ">
                        <div class="spin-wrap">
                           <img class="img-fluid header-1-img" src="<?php the_field('innovation_inner_image'); ?>">
                           <img id="gear1-moblie" class="img-fluid spin" src="<?php the_field('innovation_image'); ?>">
                           <h6 class="header-1"><?php the_field('innovation_heading'); ?></h6>
                           <svg id="chainam" class="chain_tam" height="210" width="500">
                              <line x1="0" y1="0" x2="200" y2="200" style="stroke:rgb(51,51,51);stroke-width:2" />
                           </svg>
                           <!-- <svg id="chainam" class="chain_tam ipad-changes" height="210" width="500">
                        <line x1="0" y1="0" x2="153" y2="238" style="stroke:rgb(51,51,51);stroke-width:2" />
                        </svg> -->
                        </div>
                     </div>
                     <div id="gear-2-mobile" class="col-12 spinner gear-2-spinner text-right">
                        <div class="spin-wrap">
                           <img class="img-fluid spin" src="<?php the_field('technical_image'); ?>">
                           <img class="img-fluid header-2-img" src="<?php the_field('technical_inner_image'); ?>">
                           <h6 class="header-2"><?php the_field('technical_heading'); ?></h6>
                           <svg id="chain" class="chain_tam" height="210" width="500">
                              <line x1="300" y1="0" x2="200" y2="200" style="stroke:rgb(72,104,121);stroke-width:2" />
                           </svg>
                        </div>
                     </div>
                     <div id="gear-3-mobile" class="col-12 spinner gear-3-spinner text-left">
                        <div class="spin-wrap">
                           <img id="gear3-moblie" class="img-fluid spin" src="<?php the_field('knowledge_image'); ?>">
                           <img class="img-fluid header-3-img" src="<?php the_field('knowledge_inner_image'); ?>">
                           <h6 class="header-3"><?php the_field('knowledge_heading'); ?></h6>
                           <svg id="chain_am" class="chain_amt in_visible" height="210" width="500">
                              <line x1="200" y1="0" x2="200" y2="200" style="stroke:rgb(51,51,51);stroke-width:2" />
                           </svg>
                           <svg id="chain_am" class="chain_amt display-block" height="500" width="500">
                              <line x1="200" y1="0" x2="200" y2="540" style="stroke:rgb(51,51,51);stroke-width:2" />
                           </svg>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Our Role End -->
      <!-- Our Impacts Start -->
      <?php
      // check if the repeater field has rows of data
      if (have_rows('intent_meets_impacts')) :

      ?>
         <section data-toggle="tooltip" data-bs-tooltip="" id="meets-impact" class="impact-bg jumbotron" style="background-image: url(<?php the_field('intent_meets__impacts_background_image'); ?>);margin-top: 7%;">
            <div class="container-fluid">
               <div class="row">
                  <div class="container">
                     <div class="row">
                        <div class="col">
                           <h1 data-aos="fade-up" class="text-white text-center"><?php the_field('intent_meets_impacts_heading', false, false); ?></h1>
                        </div>
                     </div>
                     <div class="row counter" id="inview-example">
                        <?php
                        // loop through the rows of data
                        while (have_rows('intent_meets_impacts')) : the_row();
                        ?>
                           <div class="col-12 col-md-3 mt-lg-5 mt-3 impact-wrap counter" data-aos="fade-up">
                              <h1 class="timer count" data-count="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></h1>
                              <?php the_sub_field('content'); ?>
                           </div>
                        <?php
                        // loop through the rows of data
                        endwhile;
                        ?>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      <?php
      endif;
      ?>
      <!-- Our Impacts End -->
      <!-- Our Community Starts -->
      <?php
      // check if the repeater field has rows of data
      if (have_rows('community_speaks')) :
      ?>
         <section id="community-speak" class="">
            <h1 data-aos="fade-up" class="mb-3"><?php the_field('community_speaks_heading', false, false); ?></h1>
            <div class="container" data-aos="fade-up" data-aos-duration="1500">
               <div class="swiper-container community-wrapper">
                  <div class="swiper-wrapper">
                     <?php
                     // loop through the rows of data
                     while (have_rows('community_speaks')) : the_row();
                     ?>
                        <div class="swiper-slide">
                           <div class="row">
                              <div class="col-12 col-md-6">
                                 <img class="img-fluid" src="<?php the_sub_field('image'); ?>">
                              </div>
                              <div class="col-12 col-md-6 mt-lg-2">
                                 <div class="community-content">
                                    <?php the_sub_field('content_1'); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php
                     endwhile;
                     ?>
                  </div>
               </div>
               <div class="swiper-pagination mt-lg-3"></div>
            </div>
         </section>
      <?php
      endif;
      ?>
      <!-- Our Community End -->
      <!-- Our Milestone Start -->
      <?php
      // check if the repeater field has rows of data
      if (have_rows('our_milestone')) :
      ?>
         <section id="our-milestone" class="our-milestone-bg unique-dna" style="background-image: url(<?php the_field('our_milestone_background_image'); ?>);">
            <h1 class="text-white mt-3"><?php the_field('our_milestone_heading', false, false); ?></h1>
            <div class="container-fluid mt-4" data-aos="fade-up">
               <div class="row">
                  <div class="col-md-3 align-self-center  bounce animated">
                     <div class="journey-content arrow journey-scroll right-side">
                        <h1 class="text-white prev-btn"></h1>
                        <?php
                        $i = 0;
                        // loop through the rows of data
                        while (have_rows('our_milestone')) : the_row();

                        ?>
                           <h1 class="text-white prev-btn"><?php the_sub_field('years'); ?></h1>
                        <?php
                           $i--;
                        endwhile;
                        ?>
                     </div>
                  </div>
                  <div class="col-md-6 journey-image" id="bg-border">
                     <?php
                     // loop through the rows of data
                     while (have_rows('our_milestone')) : the_row();

                     ?>
                        <div class="row">
                           <div class="col col-12">
                              <div class="card mb-3">
                                 <div class="card-body p-0">
                                    <img class="img-fluid" src="<?php the_sub_field('image'); ?>">
                                    <div class="card-text text-center my-3">
                                       <h1><?php the_sub_field('years'); ?></h1>
                                       <?php the_sub_field('content'); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php
                     endwhile;
                     ?>
                  </div>
                  <div class="col-md-3 align-self-center">
                     <div class="journey-content arrow journey-scroll left-side">
                        <?php
                        $i = 0;
                        // loop through the rows of data
                        while (have_rows('our_milestone')) : the_row();
                           if ($i != 0) :
                        ?>
                              <h1 class="text-white next-btn"><?php the_sub_field('years'); ?></h1>
                        <?php
                           endif;
                           $i++;
                        endwhile;
                        ?>
                        <h1 class="text-white next-btn"></h1>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      <?php
      endif;

      ?>
      <!-- Our Milestone End -->
      <!-- New Updates Start -->
      <section id="news-and-updates" class="news-updates unique-dna">
         <?php if (get_field('news_updates_heading')) : ?>
            <h1 class="my-4" data-aos="fade-up"><?php the_field('news_updates_heading', false, false); ?></h1>
         <?php
         endif;
         ?>
         <div class="container-fluid">
            <div class="row">
               <div class="container">
                  <div class="row">
                     <?php
                     $args = array(
                        'post_type' => 'newsandupdates',
                        'posts_per_page' => '4',
                        'order' => 'DESC'
                     );
                     $incre = 1;
                     $the_query = new WP_Query($args);
                     if ($the_query->have_posts()) :
                        while ($the_query->have_posts()) :
                           $the_query->the_post();
                           if ($incre === 1) :
                     ?>
                              <div class="col-12 col-md-6 mb-4 new-wrapper" data-aos="fade-up">
                                 <div class="card text-left">
                                    <div class="card-body">
                                       <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>">
                                       <div class="card-text mt-lg-5 mt-4">
                                          <p><?php echo get_the_date(); ?></p>
                                          <p><?php echo get_the_title(); ?></p>
                                          <a href="#">
                                             <p class="text-orange">Read More</p>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           <?php
                           endif;
                           if ($incre == 1) :
                           ?>
                              <div class="col-12 col-md-6 news-wrapper" data-aos="fade-up">
                              <?php
                           endif;
                           if ($incre == 2 || $incre == 3 || $incre == 4) :
                              ?>
                                 <div class="card">
                                    <div class="row no-gutters">
                                       <div class="col-4"><img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></div>
                                       <div class="col-8 card-text">
                                          <div class="content-wrapper">
                                             <p><?php echo get_the_date('jS M, Y'); ?></p>
                                             <p><?php echo get_the_title(); ?></p>
                                             <a href="#">
                                                <p class="text-orange">Read More</p>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php
                           endif;
                           if ($incre == 5) :
                              ?>
                              </div>
                     <?php
                           endif;
                           $incre++;
                        endwhile;
                     endif;
                     /* Restore original Post Data */
                     wp_reset_postdata();
                     ?>
                     <div class="col-12">
                        <?php if (get_field('our_view_text')) : ?>
                           <a class="custom-btn my-4 button2" data-text="<?php the_field('our_view_text') ?>" href="<?php echo site_url('new-updates') ?>">
                              <span class="btn" role="button" target="_parent"><?php the_field('our_view_text') ?></span>
                           </a>
                        <?php endif; ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- New Updates End -->
      <!-- Work With Us Start -->
      <section class="text-center mt-5 our-milestone-bg" id="work-with-us" style="background-image: url('<?php the_field('work_with_us_background_image'); ?>');">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h1 class="text-white" data-aos="fade-up"><?php the_field('work_with_us_heading', false, false); ?></span></h1>
                  <?php the_field('work_with_us_content') ?>
                  <a class="custom-btn my-4 button2" data-text="<?php the_field('work_with_us_text') ?>" href="<?php echo site_url('work-with-us') ?>">
                     <span class="btn" role="button" target="_parent"><?php the_field('work_with_us_text') ?></span>
                  </a>
               </div>
            </div>
         </div>
      </section>
      <!-- Work With Us End -->
      <!-- Our Patners Start -->
      <section id="our-partners" class="spacing-header">
         <div class="container mt-4">
            <div class="row">
               <div class="col-12">
                  <?php if (get_field('news_updates_heading')) : ?>
                     <h1 class="my-4" data-aos="fade-up"><?php the_field('our_partners_heading', false, false); ?></h1>
                  <?php
                  endif;
                  ?>
               </div>
            </div>
            <div class="row mt-3 our-partners" data-aos="fade-up">
               <?php
               // check if the repeater field has rows of data
               if (have_rows('our_partners')) :
                  // loop through the rows of data
                  while (have_rows('our_partners')) : the_row();
               ?>
                     <div class="col-12 col-md-2"><img src="<?php the_sub_field('image') ?>"></div>
               <?php
                  endwhile;
               endif;
               ?>
               <?php if (get_field('our_view_text')) : ?>
               <?php endif; ?>
            </div>
            <div class="row mt-3">
               <div class="col-12 col-md-7 offset-md-5">
                  <a class="custom-btn my-4 button2 text-center" data-text="<?php the_field('our_view_text') ?>" href="<?php echo site_url('partners') ?>">
                     <span class="btn" role="button" target="_parent"><?php the_field('our_view_text') ?></span>
                  </a>
               </div>
            </div>
         </div>
      </section>
      <!-- Our Patners End -->
<?php
   endwhile;
endif;
get_footer();
?>