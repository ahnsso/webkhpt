<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
if (have_posts()) :
    while (have_posts()) : the_post();
?>
        <section id="news-updates" class="banner" style="background-image: url(<?php the_field('news_banner'); ?>);background-position: center;background-size: cover;background-repeat: no-repeat;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 news_updates">
                        <h1><?php echo get_the_title(); ?></h1>
                        <hr class="news-update">
                    </div>
                </div>
            </div>
        </section>
<?php
    endwhile;
endif;
get_footer();
?>