<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
$terms = get_terms(array(
   'taxonomy' => 'our-teams-taxonomy',
   'hide_empty' => false,
));

$office_type = get_field_object("office_type")['choices'];


$selectedDepartment = '';
if (isset($_GET['department'])) {
   $selectedDepartment = $_GET['department'];
}

global $post;
?>
<section id="contact-us" class="banner">
   <div class="container-fluid container-sec-one">
      <div class="row no-gutters banner-wrap" style="
         background-image: url(<?php the_field('our_team_banner', 'option'); ?>);background-repeat: no-repeat;
         background-size: cover;
         background-position: center;
         ">
         <div class="col-12 text-center">
            <div class="health-care">
               <h1 data-aos="fade-up"><?php the_field('our_team_heading', 'option'); ?></h1>
               <hr style="margin-left: 45%;">
            </div>
         </div>
      </div>
   </div>
</section>
<section id="team-filter-sec">
   <div class="container">
      <form id="team-filter" class="form-inline" method="get">
         <div class="form-row form-right">
            <div class="form-group">
               <p class="select-wrapper">
                  Filter by Unit:&nbsp;
                  <span>
                     <select class="form-control filter-control exampleFormControlSelect2" name="department">
                        <option value="" selected="">Select one</option>
                        <?php foreach ($terms as $item) {
                        ?>
                           <option value="<?php echo $item->slug; ?>" <?php echo ($selectedDepartment == $item->slug) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                        <?php
                        } ?>
                     </select>
                  </span>
               </p>
            </div>
         </div>
         <div class="form-row form-left">
            <div class="form-group">
               <a href="#" class="btn custom-button green-btn" id="btn-submit">Filter </a>
            </div>
         </div>
      </form>
   </div>
</section>
<script>
   jQuery("#btn-submit").click(function() {
      jQuery("#team-filter").submit();
   })
</script>
<?php
if (!empty($selectedDepartment)) {
   $tax_query = array(
      array(
         'taxonomy' => 'our-teams-taxonomy',
         'field' => 'slug',
         'terms' => $selectedDepartment,
         'compare' => 'LIKE',
      ),
   );
}

$args = array(
   'post_type' => 'our-teams',
   'posts_per_page' => '-1',
   'order' => 'DESC',
   'tax_query' => $tax_query,
);
$the_query = new WP_Query($args);
?>
<section id="Filters-role">
   <div class="container">
      <div class="row">
         <?php
         if ($the_query->have_posts()) :
            $incre = 1;
            while ($the_query->have_posts()) :
               $the_query->the_post();
         ?>
               <div class="col-12 col-md-3" <?php echo $incre == 1 ? 'data-aos="fade-up"' : 'data-aos="fade-up"'; ?>>
                  <a role="button" href="#" data-toggle="modal" data-target="#myModal<?php echo $incre; ?>">
                     <div class="card">
                        <div class="card-body p-0"><?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?></div>
                        <div class="card-text">
                           <h2><?php echo get_the_title(); ?></h2>
                           <h3><?php the_field('title'); ?></h3>
                        </div>
                        <hr>
                     </div>
                  </a>
               </div>
               <div class="modal fade" role="dialog" tabindex="-1" id="myModal<?php echo $incre; ?>">
                  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                     <div class="modal-content">
                        <div class="modal-header"><button type="button" class="close btn-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-5">
                                 <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                                 <div class="teamtitle">
                                    <h3 class="team-member-name"><?php echo get_the_title(); ?></h3>
                                 </div>
                              </div>
                              <div class="col-md-7 mt-3 content-modal">
                                 <?php the_field('content'); ?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer"></div>
                     </div>
                  </div>
               </div>
            <?php
               $incre++;
            endwhile;
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
         <?php else : ?>
            <?php _e('Something went wrong'); ?>
         <?php endif; ?>
      </div>
   </div>
</section>
<?php
get_footer();
?>