<!--Banner start-->
<!--<section class="banner">-->
<!--    --><?php
//    $attr = array(
//        "class" => "img-fluid d-none d-md-block"
//    );
//    if(get_field('image')):
//        echo wp_get_attachment_image(get_field('image'), 'full', false, $attr);
//    endif;
//    if(get_field('mobile')):
//        $attr["class"] = "img-fluid d-block d-md-none";
//        echo wp_get_attachment_image(get_field('mobile_image'), 'full', false, $attr);
//    endif;
//    the_title('<h1>', '</h1>');
//    ?>
<!--</section>-->
<!--Banner End-->

<section id="contact-us" class="banner">
            <div class="container-fluid container-sec-one">
                <div class="row no-gutters banner-wrap" 
                style="background-image: url(<?php the_field('banner_image'); ?>);
                background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
">
                    <div class="col-12 text-center">
                        <div class="health-care">
                            <h1 data-aos="fade-up"><?php echo get_the_title(); ?></h1>
                            <hr style="margin-left: 45%;">
                        </div>
                    </div>
                </div>
            </div>
        </section>