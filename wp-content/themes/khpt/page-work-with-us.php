<?php
   /**
    * The main template file
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * E.g., it puts together the home page when no home.php file exists.
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package WordPress
    * @subpackage Ihat
    * @since 1.0.0
    */
   get_header();
   if (have_posts()) :
       while (have_posts()) : the_post();
   ?>
<!-- Our Work with US -->
<?php get_template_part('template/banner', 'inside'); ?>
<!-- Our Work With Us -->
<section id="work-with-us-filter">
   <div class="container">
      <h2 class="text-orange mb-5"><?php the_field('current_opening_heading', false, false); ?></h2>
   </div>
</section>
<section id="current-opening" class="mt-lg-2">
   <div class="container">
      <?php
         $args = array(
             'post_type' => 'vacancies',
             'posts_per_page' => '-1',
             'order' => 'DESC'
         );
         $the_query = new WP_Query($args);
         if ($the_query->have_posts()) :
             while ($the_query->have_posts()) :
                 $the_query->the_post();
         ?>
      <div class="row my-3" data-aos="fade-up">
         <div class="col-12 col-md-8 current-opening-wrap">
            <?php echo get_field('vacancies_heading'); ?>
            <?php
               $target = "_self";
               if (get_field('new_tab')) :
                   $target = "_blank";
               endif;
               ?>
            <a class="download-pdf" href="<?php echo get_field('download_pdf_link'); ?>" target="<?php echo $target; ?>" download=""><?php echo get_field('label_download_pdf'); ?><span><i class="fa fa-file-pdf-o"></span></i></a>
            <h3 class="d-flex">Last Application Date -<span><?php echo get_field('date_heading'); ?></span></h3>
         </div>
         <div class="col-12 col-md-4 apply-wrap">
            <?php
               $target = "_self";
               if (get_field('new_tab')) :
                   $target = "_blank";
               endif;
               ?>
            <a class="btn custom-btn green-btn apply-btn" href="<?php echo the_permalink(); ?>" target="<?php echo $target; ?>">
            <span><?php echo get_field('label_text'); ?></span>
            </a>
         </div>
         <div class="col-12 border-hr">
            <hr>
         </div>
      </div>
      <?php
         endwhile;
         else :
         /* Restore original Post Data */
         
         ?>
      <div class="row my-3">
         <div class="col-12 col-md-8 current-opening-wrap">
            There are currently no openings available.
         </div>
      </div>
      <?php
         endif;
         wp_reset_postdata();
         ?>
   </div>
</section>
<section id="community-speak" class="my-5" style="background-image: url(<?php the_field('community_speak_desktop_banner'); ?>);background-position: center;background-size: cover;background-repeat: no-repeat;    padding: 100px 0;">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-5">
            <div class="speak-content text-white" data-aos="fade-up">
               <h1><?php the_field('community_speak_heading'); ?></h1>
               <p><?php the_field('community_speak_content'); ?></p>
               <div class="custom-btn news" data-text="<?php the_field('community_text_view'); ?>"><a class="btn submit-btn text-white" role="button" href="#"><?php the_field('community_text_view'); ?></a></div>
            </div>
         </div>
      </div>
   </div>
</section>
<section id="accordian-sec" class="mb-5">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-5">
            <div class="row">
               <div class="col-12 accordion-header mb-4" data-aos="fade-up">
                  <h1><?php the_field('walk_in_interveiws_heading'); ?></h1>
               </div>
               <div class="col-12 accordian-sec" data-aos="fade-up">
                  <div role="tablist" id="accordion-1" class="accordian-wrap">
                     <?php
                        // check if the repeater field has rows of data
                        if (have_rows('walk_in_interveiws')) :
                            $i = 0;
                            // loop through the rows of data
                            while (have_rows('walk_in_interveiws')) : the_row();
                        ?>
                     <div class="card">
                        <div class="card-header greek card-border" role="tab">
                           <h5 class="mb-0">
                              <a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-<?php echo $i; ?>" href="#accordion-1 .item-<?php echo $i; ?>" class="card-header-wrap">
                              <?php the_sub_field('heading'); ?>
                              </a>
                           </h5>
                        </div>
                        <div <?php echo $i == 0 ? 'class="acc-wrapper show collapse item-' . $i . '"' : 'class="acc-wrapper collapse item-' . $i . '"'; ?> role="tabpanel" data-parent="#accordion-1">
                           <div class="card-body">
                              <div class="accordion-span d-flex">
                                 <?php the_sub_field('date_heading'); ?>
                                 <?php the_sub_field('timings'); ?>
                              </div>
                              <a href="#">
                                 <h6><?php the_sub_field('get_heading'); ?>&nbsp; &gt;&gt;</h6>
                              </a>
                           </div>
                        </div>
                     </div>
                     <?php
                        $i++;
                        endwhile;
                        endif;
                        ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12 col-md-5 offset-md-2" data-aos="fade-up">
            <div class="row">
               <div class="col-12 accordion-header mb-4 mobile-top">
                  <h1><?php the_field('rpfstenders_heading'); ?></h1>
               </div>
               <div class="col-12 accordian-sec">
                  <div role="tablist" id="accordion-2" class="accordian-wrap">
                     <?php
                        // check if the repeater field has rows of data
                        if (have_rows('rpfstenders')) :
                            $i = 1;
                            // loop through the rows of data
                            while (have_rows('rpfstenders')) : the_row();
                        ?>
                     <div class="card">
                        <div class="card-header greedy card-borderr" role="tab">
                           <h5 class="mb-0">
                              <a data-toggle="collapse" <?php echo $i == 1 ? 'aria-expanded="true"' : 'aria-expanded="false"'; ?> aria-controls="accordion-2 .item-<?php echo $i; ?>" href="#accordion-2 .item-<?php echo $i; ?>" class="card-header-wrap">
                              <?php the_sub_field('heading'); ?>
                              </a>
                           </h5>
                        </div>
                        <div <?php echo $i == 1 ? 'class="acc-wrapper show collapse item-' . $i . '"' : 'class="acc-wrapper collapse item-' . $i . '"'; ?> role="tabpanel" data-parent="#accordion-2">
                           <div class="card-body">
                              <div class="accordion-span d-flex">
                                 <h3><?php the_sub_field('date_heading', false, false); ?></h3>
                              </div>
                              <a class="btn search-btn apply-btn" href="#" target="_blank"><?php the_sub_field('label_text'); ?></a>
                           </div>
                        </div>
                     </div>
                     <?php
                        $i++;
                        endwhile;
                        endif;
                        ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php
   endwhile;
   endif;
   get_footer();
   ?>
COPY TO CLIPBOARD	