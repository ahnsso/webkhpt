<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <!--Banner start-->
    <section class="banner">
        <img src="<?php the_field('image'); ?>" class="img-fluid d-none d-md-block" alt="Banner Desktop">
        <img src="<?php the_field('mobile_image'); ?>" class="img-fluid d-block d-md-none" alt="Banner Desktop">
        <h1><?php echo get_the_title(); ?></h1>
    </section>
    <!--Banner End-->

    <!--Article start-->
    <section class="page-article">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Article End-->

<?php
endwhile;
else:?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>Sorry no content found.</p>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php
get_footer();
?>
