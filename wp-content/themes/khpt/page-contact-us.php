<?php
   /**
    * The main template file
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * E.g., it puts together the home page when no home.php file exists.
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package WordPress
    * @subpackage Ihat
    * @since 1.0.0
    */
   get_header();
   if (have_posts()) :
       while (have_posts()) : the_post();
   ?>
<!-- Contact Us Banner Start-->
<?php get_template_part('template/banner', 'inside'); ?>
<!-- Contact Us Banner End-->
<!-- Contact Us Detail Start -->
<section id="form-map" class="contact-us">
   <div class="container">
      <div class="row">
         <?php if (shortcode_exists('contact-form-7'))
            {
               ?>
         <div class="col-12 col-md-6" data-aos="fade-up">
            <?php  
               echo do_shortcode('[contact-form-7 id="255" title="Contact Us"]');
                ?>
         </div>
         <?php
            }
            ?>
         <?php if(get_field('google_map')):
            ?>
         <div class="col-12 col-md-5 offset-md-1 map-responsive" data-aos="fade-up">
            <p><iframe style="border: 0;" src="<?php the_field('google_map'); ?>" width="100%" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
         </div>
         <?php
            endif;
            ?>
         <div class="col">
            <hr class="contact">
         </div>
      </div>
   </div>
</section>
<!-- Contact Us Detail End -->
<!-- Contact Us Google Map Start-->
<section id="contact-address" class="pt-3 pb-5" data-aos="fade-up">
   <div class="container">
      <div class="row">
         <?php if(get_field('find_us_at_')):
            ?>
         <div class="col-12 col-md-4 find-us">
            <h3><?php the_field('find_us_at_heading'); ?></h3>
            <p><?php the_field('find_us_at_'); ?></p>
         </div>
         <?php
            endif;
            ?>
         <div class="col-12 col-md-4">
            <h3><?php the_field('write_to_us_heading'); ?></h3>
            <h6 class="contact-link"><a href="#" target="_blank"><?php the_field('write_to_us:'); ?></a></h6>
         </div>
         <div class="col-12 col-md-4">
            <h3><?php the_field('speak_to_us_heading'); ?></h3>
            <h6 class="contact-link"><a href="#"><?php the_field('speak_to_us:'); ?></a></h6>
         </div>
      </div>
   </div>
</section>
<!-- Contact Us  Map End-->
<?php
   endwhile;
   endif;
   get_footer();
   ?>